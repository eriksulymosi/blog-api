<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();

        $user->email = 'erik.sulymosi@gmail.com';
        $user->name = 'Erik Sulymosi';
        $user->password = Hash::make('awsdawsd');

        $user->save();
    }
}
