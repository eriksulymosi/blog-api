<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Resource
{
    use SoftDeletes;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'author' => new User($this->author),
            'title' => $this->title,
            'body' => $this->body,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
            'comments' => Comment::collection($this->whenLoaded('comments'))
        ];
    }
}
