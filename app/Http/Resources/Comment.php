<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Resource
{
    use SoftDeletes;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'author' => new User($this->author),
            'body' => $this->body,
            'created_at' => $this->created_at->toDateTimeString(),
        ];
    }
}
