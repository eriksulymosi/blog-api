<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Http\Resources\Post as PostResource;
use App\Http\Requests\StorePost;
use App\Http\Requests\UpdatePost;
use App\Http\Requests\DeletePost;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PostResource::collection(Post::with(['author'])->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $post = new Post();

        $post->slug = $request->input('slug', str_slug($request->input('title') . '-' . time()));
        $post->title = $request->input('title');
        $post->body = $request->input('body');

        $request->user()->posts()->save($post);

        return new PostResource($post->load(['author']));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return new PostResource($post->load(['author', 'comments']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdatePost  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePost $request, Post $post)
    {
        $post->slug = $request->input('slug', $post->slug);
        $post->title = $request->input('title', $post->title);
        $post->body = $request->input('body', $post->body);

        $post->save();

        return (new PostResource($post->load(['author'])))->response()->setStatusCode(201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Http\Requests\DeletePost  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeletePost $request, Post $post)
    {
        $post->delete();

        return response(null, 204);
    }
}
