<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;
use App\Http\Resources\Comment as CommentResource;
use App\Http\Requests\StoreComment;
use App\Http\Requests\DeleteComment;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        return CommentResource::collection($post->comments()->with(['author'])->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreComment  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComment $request, Post $post)
    {
        $comment = new Comment();

        $comment->body = $request->input('body');
        $comment->user_id = $request->user()->id;

        $post->comments()->save($comment);

        return new CommentResource($comment->load(['author']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Http\Requests\DeleteComment $request
     * @param  \App\Models\Post  $post
     * @param  App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteComment $request, Post $post, Comment $comment)
    {
        $comment->delete();

        return response(null, 204);
    }
}
