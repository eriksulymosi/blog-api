<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\Post;

class UpdatePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $post = $this->route('post');
        return $post && $this->user()->can('update', $post);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $post = $this->route('post');

        return [
            'slug' => [
                'min:3',
                'max:255',
                Rule::unique('posts')->ignore($post->id),
                'alpha_dash'
            ],
            'title' => [
                'required',
                'min:3',
                'max:255'
            ],
            'body' => [
                'required',
                'min:3'
            ]
        ];
    }
}
