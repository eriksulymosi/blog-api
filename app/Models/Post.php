<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function scopeSlugLike($query, $slug)
    {
        return $query->where('slug', 'like', "{$slug}%")
            ->orderBy('slug', 'desc');
    }
}
